import 'package:flutter/material.dart';

const primaryColor = Color(0xFF0C9869);
const TextColor = Color(0xFF3C4046);
const BackgroundColor = Color(0xFFF9F8FD);

const double DefaultPadding = 20.0;