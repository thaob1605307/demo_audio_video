import 'package:flutter/material.dart';
import 'package:media_player/screens/home/components/body.dart';
import 'package:scroll_app_bar/scroll_app_bar.dart';

class HomeScreen extends StatelessWidget{
  final controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: buildAppBar(context),
      body: Snap(
        controller: controller.appBar,
        child: SafeArea(child:BodyApp(controller: controller),)
      )
    );
  }
  ScrollAppBar buildAppBar (BuildContext context){
    return ScrollAppBar(
      controller: controller,
      elevation: 0,
      title:  Row(
        children: <Widget>[
          Text(
            'Hi!',
            style: Theme.of(context).textTheme.headline6.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold),
          ),
           Image.asset("assets/images/logo_smile.png",width: 50,height: 50,)
        ],
      ),
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.menu),
        onPressed: null,
      ),
    );
}

}