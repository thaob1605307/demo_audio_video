import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';

class TitleContent extends StatelessWidget {
  const TitleContent({
    Key key,
    this.title,
    this.press,
  }) : super(key: key);
  final String title;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: DefaultPadding,vertical: DefaultPadding/2),
      child: Row(
        children: <Widget>[
          TitleWithCustomUnderline(text: title),
          Spacer(),
          // CupertinoButton(
          //   padding: EdgeInsets.fromLTRB(7.0, 0.0, 7.0, 0.0),
          //   borderRadius:BorderRadius.circular(20) ,
          //   // shape: RoundedRectangleBorder(
          //   //   borderRadius: BorderRadius.circular(20),
          //   // ),
          //   color: primaryColor,
          //   onPressed: press,
          //   child: Text(
          //     "More",
          //     style: TextStyle(color: Colors.white),
          //   ),
          // ),
        ],
      ),
    );
  }
}

class TitleWithCustomUnderline extends StatelessWidget {
  const TitleWithCustomUnderline({
    Key key,
    this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: DefaultPadding / 4),
            child: Text(
              text,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: primaryColor),
            ),
          ),
          // Positioned(
          //   bottom: 0,
          //   left: 0,
          //   right: 0,
          //   child: Container(
          //     margin: EdgeInsets.only(right: DefaultPadding / 4),
          //     height: 2,
          //     color: primaryColor.withOpacity(0.2),
          //   ),
          // )
        ],
      ),
    );
  }
}