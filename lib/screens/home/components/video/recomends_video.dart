import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:media_player/screens/home/components/video/video_model.dart';
import 'package:video_player/video_player.dart';

import '../../../../constants.dart';

class RecommendsVideo extends StatefulWidget {
  final Size size;
  const RecommendsVideo({Key key, this.size}) : super(key: key);
  @override
  RecommendsVideoState createState() => RecommendsVideoState();
}
// class RecommendsVideoState extends State<RecommendsVideo> {
//
//   @override
//   Widget build(BuildContext context) {
//     return  Container(
//       height: widget.size.height * 0.4,
//       child: ListView(
//         scrollDirection: Axis.horizontal,
//         padding: EdgeInsets.only(left: DefaultPadding,right: DefaultPadding),
//         children: listVideo.map((e) => ListItem(url: e.url)).toList()
//       )
//     );
//   }
//   // Widget buildItem (VideoModel videoModel){
//   //   return Container(
//   //     child: AspectRatio(
//   //         aspectRatio: 1,
//   //         child: BetterPlayer.network(
//   //           videoModel.url,
//   //           betterPlayerConfiguration: BetterPlayerConfiguration(
//   //           ),))
//   //   );
//   //
//   // }
//   void dispose() {
//     super.dispose();
//   }
// }
// class ListItem extends StatefulWidget {
//
//  final String url;
//  ListItem({this.url});
//   @override
//   ListItemState createState() => ListItemState();
// }
//
// class ListItemState extends State<ListItem> {
//  // BetterPlayerController _betterPlayerController;
//   @override
//   Widget build(BuildContext context) {
//     return  AspectRatio(
//       aspectRatio: 16 / 9,
//       child: BetterPlayerListVideoPlayer(
//         BetterPlayerDataSource(
//             BetterPlayerDataSourceType.network, widget.url),
//         key: Key(widget.url.hashCode.toString()),
//         playFraction: 0.8,
//       ),
//     );
//   }
//   void dispose() {
//     super.dispose();
//   }
//
// }
//



class RecommendsVideoState extends State<RecommendsVideo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.size.height * 0.4,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: listVideo.map((e) => ChewieListItem(
          press: () {},
          looping: true,
          videoPlayerController: VideoPlayerController.network(
           e.url
          ),
        ),).toList()
      ),
    );
  }
}
class ChewieListItem extends StatefulWidget {
  // This will contain the URL/asset path which we want to play
  final VideoPlayerController videoPlayerController;
  final bool looping;
  final Function press;

  ChewieListItem({
    @required this.videoPlayerController,
    this.looping,
    this.press,
    Key key,
  }) : super(key: key);

  @override
  _ChewieListItemState createState() => _ChewieListItemState();
}

class _ChewieListItemState extends State<ChewieListItem> {
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    _chewieController = ChewieController(
      videoPlayerController: widget.videoPlayerController,
      aspectRatio: 3/2,
      // widget.videoPlayerController.value.aspectRatio,
      // 16 / 9,
      allowFullScreen: true,
      deviceOrientationsOnEnterFullScreen: [
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
      allowMuting: true,
      autoInitialize: true,
      allowPlaybackSpeedChanging: false,
      looping: widget.looping,

      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );

  }

  @override
  Widget build(BuildContext context) {
    _chewieController.setVolume(0.5);
    return GestureDetector(
      onTap: widget.press,
      child: Container(
          child: Chewie(
            controller: _chewieController,
          ),
        ),
    );

  }
  @override
  void dispose() {
    if(widget.videoPlayerController.value.isPlaying) {
      widget.videoPlayerController.pause();
    }
    super.dispose();
  }
}


// class ChewieListItem extends StatefulWidget {
//   // This will contain the URL/asset path which we want to play
//   final VideoPlayerController videoPlayerController;
//   final bool looping;
//   final Function press;
//
//   ChewieListItem({
//     @required this.videoPlayerController,
//     this.looping,
//     this.press,
//     Key key,
//   }) : super(key: key);
//
//   @override
//   _ChewieListItemState createState() => _ChewieListItemState();
// }
//
// class _ChewieListItemState extends State<ChewieListItem> {
//   ChewieController _chewieController;
//
//   @override
//   void initState() {
//     super.initState();
//     _chewieController = ChewieController(
//       videoPlayerController: widget.videoPlayerController,
//       aspectRatio: 1,
//       // widget.videoPlayerController.value.aspectRatio,
//       // 16 / 9,
//       allowFullScreen: true,
//       deviceOrientationsOnEnterFullScreen: [
//         DeviceOrientation.landscapeRight,
//         DeviceOrientation.landscapeLeft,
//         DeviceOrientation.portraitUp,
//         DeviceOrientation.portraitDown,
//       ],
//       allowMuting: true,
//       autoInitialize: true,
//       allowPlaybackSpeedChanging: false,
//       looping: widget.looping,
//       errorBuilder: (context, errorMessage) {
//         return Center(
//           child: Text(
//             errorMessage,
//             style: TextStyle(color: Colors.white),
//           ),
//         );
//       },
//     );
//
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: widget.press,
//       child: Container(
//         child: Chewie(
//           controller: _chewieController,
//         ),
//       ),
//     );
//   }
//   @override
//   void dispose() {
//     if(widget.videoPlayerController.value.isPlaying) {
//       widget.videoPlayerController.pause();
//     }
//     super.dispose();
//   }
// }
