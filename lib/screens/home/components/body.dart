import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:media_player/screens/home/components/audio/audio_bloc/audio_bloc.dart';
import 'package:media_player/screens/home/components/header_with_searchbox.dart';
import 'package:media_player/screens/home/components/title_content.dart';
import 'audio/audio_repository/audio_model_factory.dart';
import 'audio/audio_repository/audio_provider.dart';
import 'audio/audio_repository/audio_repository.dart';
import 'audio/recomends_audio.dart';
import 'video/recomends_video.dart';

class BodyApp extends StatelessWidget{
  final controller ;
  BodyApp({Key key,this.controller });
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
     return SingleChildScrollView(
       controller: controller,
       child:Column (
         children: [
           HeaderSearchBox(size: size ),
           TitleContent(title:"Video",press: (){},),
           RecommendsVideo(size: size,),
           TitleContent(title:"Audio",press: (){},),
           MultiRepositoryProvider(
               providers: [
                 RepositoryProvider<AudioPlayerRepository>(
                   create: (context)=>AudioProvider(
                     audioPlayerModels: AudioPlayerModelFactory.getAudioPlayerModels()
                   ),
                 )
               ],
               child: MultiBlocProvider(
                 providers: [
                   BlocProvider<AudioBloc>(create:(BuildContext context)=> AudioBloc(
                     assetsAudioPlayer: AssetsAudioPlayer.newPlayer(),
                     audioPlayerRepository: RepositoryProvider.of<AudioPlayerRepository>(context)) )
                 ],
                 child:RecommendsAudio(size: size) ,
               )
           )
           // RecommendsAudio(size: size,),
           // SizedBox(height: DefaultPadding),
         ],
       ) ,
     );

  }

}