import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'audio_bloc/audio_bloc.dart';
import 'audio_bloc/audio_event.dart';
import 'audio_bloc/audio_state.dart';
import 'audio_model.dart';

class PlayerWidget extends StatelessWidget {
  const PlayerWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AudioBloc, AudioPlayerState>(
      builder: (context, state) {
        if (state is AudioPlayerInitial || state is AudioPlayerReady) {
          return SizedBox.shrink();
        }
        if (state is AudioPlayerPlaying) {
          return _showPlayer(context, state.playingEntity);
        }
        if (state is AudioPlayerPaused) {
          return _showPlayer(context, state.pausedEntity);
        } else {
          return SizedBox.shrink();
        }
      },
    );
  }

  Widget _showPlayer(BuildContext context, AudioModel model) {
    return
      Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            color: Colors.grey.shade200,
            child: ListTile(
              leading: setLeading(model),
              title: setTitle(model),
              subtitle: setSubtitle(model),
              contentPadding: EdgeInsets.symmetric(vertical: 4, horizontal: 16),
              trailing: IconButton(
                icon: setIcon(model),
                onPressed: setCallback(context, model),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget setIcon(AudioModel model) {
    if (model.isPlaying)
      return Icon(Icons.pause);
    else
      return Icon(Icons.play_arrow);
  }

  Widget setLeading(AudioModel model) {
    return new Image.asset(model.audio.metas.image.path);
  }

  Widget setTitle(AudioModel model) {
    return Text(model.audio.metas.title);
  }

  Widget setSubtitle(AudioModel model) {
    return Text(model.audio.metas.artist);
  }

  Function setCallback(BuildContext context, AudioModel model) {
    if (model.isPlaying)
      return () {
        BlocProvider.of<AudioBloc>(context)
            .add(TriggeredPauseAudio(model));
      };
    else
      return () {
        BlocProvider.of<AudioBloc>(context)
            .add(TriggeredPlayAudio(model));
      };
  }
}
