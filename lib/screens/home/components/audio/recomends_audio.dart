
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:media_player/screens/home/components/audio/audio_bloc/audio_bloc.dart';
import 'package:media_player/screens/home/components/audio/audio_bloc/audio_event.dart';
import 'package:media_player/screens/home/components/audio/audio_bloc/audio_state.dart';
import 'package:media_player/screens/home/components/audio/play_audio.dart';
import 'audio_model.dart';

class RecommendsAudio extends StatelessWidget {
  final Size size;
  const RecommendsAudio({
    Key key,this.size
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
     Container(
       height: size.height*0.5,
       child: BlocBuilder<AudioBloc,AudioPlayerState>(
         builder: (context,state){
           if(state is AudioPlayerInitial){
             BlocProvider.of<AudioBloc>(context).add(InitializeAudio());
             return Center(child: CircularProgressIndicator());
           }
           else if(state is AudioPlayerReady){
             return buildReadyTrackList(state);
           }
           else if(state is AudioPlayerPlaying){
             return buildPlayingTrackList(state);
           }
           else if(state is AudioPlayerPaused){
             return buildPausedTrackList(state);
           }
           else {
             return buildUnknownStateError();
           }

         },
       )

       // ListView(
       //   scrollDirection: Axis.horizontal,
       //   // child: Row(
       //   children: <Widget>[
       //   ],
       //   // ),
       //
       // ),
     );



  }
  Widget buildReadyTrackList(AudioPlayerReady state) {
    return ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return RecommendsAudioCard(audioPlayerModel: state.entityList[index],press: (){},);
        },
        itemCount: state.entityList.length);
  }
  Widget buildPlayingTrackList(AudioPlayerPlaying state) {
    return Stack(
      fit: StackFit.expand,
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          alignment: Alignment.topCenter,
          child: ListView.builder(
              padding: EdgeInsets.only(bottom: 124),
              itemBuilder: (BuildContext context, int index) {
                return RecommendsAudioCard(
                    audioPlayerModel: state.entityList[index]);
              },
              itemCount: state.entityList.length),
        ),
        Container(
          alignment: Alignment.bottomCenter,
          child: PlayerWidget(),
        )

      ],
    );
  }
  Widget buildPausedTrackList(AudioPlayerPaused state) {
    return Stack(
      fit: StackFit.expand,
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          alignment: Alignment.topCenter,
          child: ListView.builder(
              padding: EdgeInsets.only(bottom: 96),
              itemBuilder: (BuildContext context, int index) {
                return RecommendsAudioCard(
                    audioPlayerModel: state.entityList[index]);
              },
              itemCount: state.entityList.length),
        ),
        Container(
          alignment: Alignment.bottomCenter,
          child: PlayerWidget(),
        )
      ],
    );
  }
  Widget buildUnknownStateError() {
    return Text("Unknown state error");
  }
}

class RecommendsAudioCard extends StatelessWidget {
  final AudioModel audioPlayerModel;
  final Function press;
  const RecommendsAudioCard({Key key, this.press, this.audioPlayerModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return
      GestureDetector(
        onTap: press,
         child: ListTile(
           leading: setLeading(),
           title: setTitle(),
           subtitle: setSubtitle(),
           trailing: IconButton(
             icon: setIcon(),
             onPressed:  setCallback(context),
           ),
         )
      // )
    );
  }
  Widget setIcon() {
    if (audioPlayerModel.isPlaying)
      return Icon(Icons.pause);
    else
      return Icon(Icons.play_arrow);
  }

  Widget setLeading() {
    return new Image.asset(audioPlayerModel.audio.metas.image.path);
  }

  Widget setTitle() {
    return Text(audioPlayerModel.audio.metas.title);
  }

  Widget setSubtitle() {
    return Text(audioPlayerModel.audio.metas.artist);
  }

  Function setCallback(BuildContext context) {
    if (audioPlayerModel.isPlaying)
      return () {
        BlocProvider.of<AudioBloc>(context)
            .add(TriggeredPauseAudio(audioPlayerModel));
      };
    else
      return () {
        BlocProvider.of<AudioBloc>(context)
            .add(TriggeredPlayAudio(audioPlayerModel));
      };
  }
}



















// class RecommendsAudio extends StatelessWidget {
//   final Size size;
//   const RecommendsAudio({
//     Key key,this.size
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return
//       Container(
//         height: size.height*0.5,
//         child: ListView(
//           scrollDirection: Axis.horizontal,
//           // child: Row(
//           children: <Widget>[
//             RecommendsAudioCard(
//               image: "assets/images/image_1.png",
//               title: "Samantha",
//               country: "Russia",
//               press: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) => null,
//                   ),
//                 );
//               },
//             ),
//             RecommendsAudioCard(
//               image: "assets/images/image_2.png",
//               title: "Angelica",
//               country: "Russia",
//               press: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) => null,
//                   ),
//                 );
//               },
//             ),
//             RecommendsAudioCard(
//               image: "assets/images/image_3.png",
//               title: "Samantha",
//               country: "Russia",
//               press: () {},
//             ),
//           ],
//           // ),
//
//         ),
//       );
//
//
//
//   }
// }
//
// class RecommendsAudioCard extends StatelessWidget {
//   const RecommendsAudioCard({
//     Key key,
//     this.image,
//     this.title,
//     this.country,
//     this.press,
//   }) : super(key: key);
//
//   final String image, title, country;
//
//   final Function press;
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery
//         .of(context)
//         .size;
//     return Container(
//       margin: EdgeInsets.only(
//         left: DefaultPadding,
//         top: DefaultPadding / 2,
//         bottom: DefaultPadding * 2.5,
//       ),
//       width: size.width * 0.4,
//       child: Column(
//         children: <Widget>[
//           Image.asset(image),
//           GestureDetector(
//             onTap: press,
//             child: Container(
//               padding: EdgeInsets.all(DefaultPadding / 2),
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.only(
//                   bottomLeft: Radius.circular(10),
//                   bottomRight: Radius.circular(10),
//                 ),
//                 boxShadow: [
//                   BoxShadow(
//                     offset: Offset(0, 10),
//                     blurRadius: 50,
//                     color: primaryColor.withOpacity(0.23),
//                   ),
//                 ],
//               ),
//               child: Row(
//                 children: <Widget>[
//                   RichText(
//                     text: TextSpan(
//                       children: [
//                         TextSpan(
//                             text: "$title\n".toUpperCase(),
//                             style: Theme
//                                 .of(context)
//                                 .textTheme
//                                 .button),
//                         TextSpan(
//                           text: "$country".toUpperCase(),
//                           style: TextStyle(
//                             color: primaryColor.withOpacity(0.5),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   Spacer(),
//                 ],
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }