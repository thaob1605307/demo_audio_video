import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:equatable/equatable.dart';

class AudioModel extends Equatable{
  final String id;
  final Audio audio;
  final bool isPlaying;

  const AudioModel({this.id, this.audio, this.isPlaying});

  @override
  List<Object> get props => [this.id, this.isPlaying];

  @override
  bool get stringify => true;

  AudioModel copyWithIsPlaying(bool isPlaying) {
    return AudioModel(id: this.id, audio: this.audio, isPlaying: isPlaying);
  }
}