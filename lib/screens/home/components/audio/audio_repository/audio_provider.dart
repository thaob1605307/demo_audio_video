
import '../audio_model.dart';
import 'audio_repository.dart';

class AudioProvider implements AudioPlayerRepository {

  final List<AudioModel> audioPlayerModels;

  AudioProvider({this.audioPlayerModels});

  @override
  Future<AudioModel> getById(String audioPlayerId) {
    return Future.value(audioPlayerModels.firstWhere((model) => model.id == audioPlayerId));
  }

  @override
  Future<List<AudioModel>> getAll() async {
    return Future.value(audioPlayerModels);
  }


  @override
  Future<List<AudioModel>> updateModel(AudioModel updatedModel) {
    audioPlayerModels[audioPlayerModels.indexWhere((element) => element.id == updatedModel.id)] = updatedModel;
    return Future.value(audioPlayerModels);
  }

  @override
  Future<List<AudioModel>> updateAllModels(List<AudioModel> updatedList) {
    audioPlayerModels.clear();
    audioPlayerModels.addAll(updatedList);
    return Future.value(audioPlayerModels);
  }
}