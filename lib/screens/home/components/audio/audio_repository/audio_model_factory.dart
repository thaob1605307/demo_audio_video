import 'package:assets_audio_player/assets_audio_player.dart';

import '../audio_model.dart';

class AudioPlayerModelFactory {
  static List<AudioModel> getAudioPlayerModels() {
    return [
      AudioModel(
          id: "1",
          isPlaying: false,
          audio: Audio("assets/audios/country.mp3",
              metas: Metas(
                id: "1",
                title: "My Country Song 1",
                artist: "Joe Doe",
                album: "Country Album",
                image: MetasImage.asset("assets/images/country.jpg"),
              ))),
      AudioModel(
          id: "2",
          isPlaying: false,
          audio: Audio("assets/audios/country.mp3",
              metas: Metas(
                id: "2",
                title: "Love Song",
                artist: "Joe Doe",
                album: "Country Album",
                image: MetasImage.asset("assets/images/country.jpg"),
              ))),
      AudioModel(
          id: "2",
          isPlaying: false,
          audio: Audio.network("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
              metas: Metas(
                id: "3",
                title: "Hello thao",
                artist: "thao",
                album: "Album",
                image: MetasImage.asset("assets/images/country.jpg"),
              ))),
      // AudioModel(
      //     id: "3",
      //     isPlaying: false,
      //     audio: Audio("assets/audios/country.mp3",
      //         metas: Metas(
      //           id: "3",
      //           title: "Dynamic",
      //           artist: "Joe Doe",
      //           album: "Country Album",
      //           image: MetasImage.asset("assets/images/country.jpg"),
      //         ))),
      // AudioModel(
      //     id: "4",
      //     isPlaying: false,
      //     audio: Audio("assets/audios/country.mp3",
      //         metas: Metas(
      //           id: "4",
      //           title: "My Other Country Song 2",
      //           artist: "Joe Doe",
      //           album: "Country Album",
      //           image: MetasImage.asset("assets/images/country.jpg"),
      //         ))),
      // AudioModel(
      //     id: "5",
      //     isPlaying: false,
      //     audio: Audio("assets/audios/country.mp3",
      //         metas: Metas(
      //           id: "5",
      //           title: "My life",
      //           artist: "Joe Doe",
      //           album: "Country Album",
      //           image: MetasImage.asset("assets/images/country.jpg"),
      //         ))),
      // AudioModel(
      //     id: "6",
      //     isPlaying: false,
      //     audio: Audio("assets/audios/country.mp3",
      //         metas: Metas(
      //           id: "6",
      //           title: "My Other Country Song 3",
      //           artist: "Joe Doe",
      //           album: "Country Album",
      //           image: MetasImage.asset("assets/images/country.jpg"),
      //         ))),
    ];
  }
}
