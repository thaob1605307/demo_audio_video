
import '../audio_model.dart';

abstract class AudioPlayerRepository {
  Future<AudioModel> getById(String audioPlayerId);
  Future<List<AudioModel>> getAll();

  Future<List<AudioModel>> updateModel(AudioModel updatedModel);
  Future<List<AudioModel>> updateAllModels(List<AudioModel> updatedList);
}