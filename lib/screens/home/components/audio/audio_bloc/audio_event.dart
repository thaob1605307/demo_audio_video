import 'package:equatable/equatable.dart';

import '../audio_model.dart';

abstract class AudioPlayerEvent extends Equatable {

  const AudioPlayerEvent();
}

class InitializeAudio extends AudioPlayerEvent {

  const InitializeAudio();

  @override
  List<Object> get props => [];
}

class TriggeredPlayAudio extends AudioPlayerEvent {

  final AudioModel audioPlayerModel;

  const TriggeredPlayAudio(this.audioPlayerModel);

  @override
  List<Object> get props => [audioPlayerModel];
}

class TriggeredPauseAudio extends AudioPlayerEvent {

  final AudioModel audioPlayerModel;

  const TriggeredPauseAudio(this.audioPlayerModel);

  @override
  List<Object> get props => [audioPlayerModel];
}

class AudioPlayed extends AudioPlayerEvent {

  final String audioModelMetaId;

  const AudioPlayed(this.audioModelMetaId);

  @override
  List<Object> get props => [audioModelMetaId];
}

class AudioPaused extends AudioPlayerEvent {

  final String audioModelMetaId;

  const AudioPaused(this.audioModelMetaId);

  @override
  List<Object> get props => [audioModelMetaId];
}

class AudioStopped extends AudioPlayerEvent {

  const AudioStopped();

  @override
  List<Object> get props => [];
}