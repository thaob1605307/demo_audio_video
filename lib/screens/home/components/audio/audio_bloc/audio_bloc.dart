import 'dart:async';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:media_player/screens/home/components/audio/audio_bloc/audio_event.dart';
import 'package:media_player/screens/home/components/audio/audio_bloc/audio_state.dart';
import 'package:media_player/screens/home/components/audio/audio_repository/audio_repository.dart';

import '../audio_model.dart';

class AudioBloc extends Bloc<AudioPlayerEvent,AudioPlayerState>{
  final AssetsAudioPlayer assetsAudioPlayer;
  final AudioPlayerRepository audioPlayerRepository;
  List<StreamSubscription> playerSubscriptions = [];
  AudioBloc({this.assetsAudioPlayer,this.audioPlayerRepository}) : super(null){
    playerSubscriptions.add(assetsAudioPlayer.playerState.listen((event) {
      _mapPlayerStateToEvent(event);
    }));
  }
  AudioPlayerState get initialState => AudioPlayerInitial();
  @override
  Stream<AudioPlayerState> mapEventToState(AudioPlayerEvent event) async* {
      if(event is InitializeAudio){
        final audioList = await audioPlayerRepository.getAll();
        yield AudioPlayerReady(audioList);
      }
      if(event is AudioPlayed){
        yield* _mapAudioPlayedToState(event);
      }
      if(event is AudioPaused){
        yield* _mapAudioPausedToState(event);
      }
      if(event is AudioStopped){
        yield* _mapAudioStoppedToState();
      }
      if(event is TriggeredPlayAudio){
        yield* _mapTriggeredPlayAudio(event);
      }
      if(event is TriggeredPauseAudio){
        yield* _mapTriggeredPausedAudio(event);
      }
  }
  @override
  // ignore: must_call_super
  Future<Function> close(){
    playerSubscriptions.forEach((element) {
      element.cancel();
    });
    return assetsAudioPlayer.dispose();
  }
  void _mapPlayerStateToEvent(PlayerState playerState) {
    if (playerState == PlayerState.stop) {
      add(AudioStopped());
    }

    else if (playerState == PlayerState.pause) {
      add(AudioPaused(assetsAudioPlayer.current.valueWrapper.value.audio.audio.metas.id));
    }

    else if (playerState == PlayerState.play) {
      add(AudioPlayed(assetsAudioPlayer.current.valueWrapper.value.audio.audio.metas.id));
    }
  }
  Stream<AudioPlayerState> _mapAudioPlayedToState(AudioPlayed event) async* {
    final List<AudioModel> currentList = await audioPlayerRepository.getAll();
    final List<AudioModel> updatedList =
    currentList
        .map((audioModel) => audioModel.audio.metas.id == event.audioModelMetaId ? audioModel.copyWithIsPlaying(true) : audioModel.copyWithIsPlaying(false))
        .toList();
    await audioPlayerRepository.updateAllModels(updatedList);
    final AudioModel currentlyPlaying = updatedList.firstWhere((model) => model.audio.metas.id == event.audioModelMetaId);
    yield AudioPlayerPlaying(currentlyPlaying, updatedList);
  }
  Stream<AudioPlayerState> _mapAudioPausedToState(AudioPaused event) async* {
    final List<AudioModel> currentList = await audioPlayerRepository.getAll();
    final List<AudioModel> updatedList =
    currentList
        .map((audioModel) => audioModel.audio.metas.id == event.audioModelMetaId ? audioModel.copyWithIsPlaying(false) : audioModel)
        .toList();
    await audioPlayerRepository.updateAllModels(updatedList);
    final AudioModel currentlyPaused = currentList.firstWhere((model) => model.audio.metas.id == event.audioModelMetaId);
    yield AudioPlayerPaused(currentlyPaused, updatedList);
  }
  Stream<AudioPlayerState> _mapAudioStoppedToState() async* {
    final List<AudioModel> currentList = await audioPlayerRepository.getAll();
    final List<AudioModel> updatedList =
    currentList
        .map((audioModel) => audioModel.isPlaying ? audioModel.copyWithIsPlaying(false) : audioModel)
        .toList();
    yield AudioPlayerReady(updatedList);
    audioPlayerRepository.updateAllModels(updatedList);
  }
Stream<AudioPlayerState> _mapTriggeredPlayAudio(TriggeredPlayAudio event) async* {
  if (state is AudioPlayerReady) {
    final AudioModel updatedModel = event.audioPlayerModel.copyWithIsPlaying(true);
    final updatedList = await audioPlayerRepository.updateModel(updatedModel);

    await assetsAudioPlayer.open(
        updatedModel.audio,
        showNotification: true
    );

    yield AudioPlayerPlaying(updatedModel, updatedList);
  }

  if (state is AudioPlayerPaused) {
    if (event.audioPlayerModel.id == (state as AudioPlayerPaused).pausedEntity.id) {
      final AudioModel updatedModel = event.audioPlayerModel.copyWithIsPlaying(true);
      final updatedList = await audioPlayerRepository.updateModel(updatedModel);

      await assetsAudioPlayer.play();

      yield AudioPlayerPlaying(updatedModel, updatedList);
    }
    else {
      final AudioModel updatedModel = event.audioPlayerModel.copyWithIsPlaying(true);
      final updatedList = await audioPlayerRepository.updateModel(updatedModel);

      await assetsAudioPlayer.open(
          updatedModel.audio,
          showNotification: true,
          respectSilentMode: true,
          headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug
      );

      yield AudioPlayerPlaying(updatedModel, updatedList);
    }
  }

  if (state is AudioPlayerPlaying) {
    final AudioModel updatedModel = event.audioPlayerModel.copyWithIsPlaying(true);
    final updatedList = await audioPlayerRepository.updateModel(updatedModel);

    await assetsAudioPlayer.open(
        updatedModel.audio,
        showNotification: true
    );

    yield AudioPlayerPlaying(updatedModel, updatedList);
  }
}
  Stream<AudioPlayerState> _mapTriggeredPausedAudio(TriggeredPauseAudio event) async* {
    final AudioModel updatedModel = event.audioPlayerModel.copyWithIsPlaying(false);
    final updatedList = await audioPlayerRepository.updateModel(updatedModel);

    await assetsAudioPlayer.pause();

    yield AudioPlayerPaused(updatedModel, updatedList);
  }

}