import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../constants.dart';

class HeaderSearchBox extends StatelessWidget{
  final Size size;
  const HeaderSearchBox({Key key,@required this.size});
  @override
  Widget build(BuildContext context) {
    return  Container(
      height: size.height*0.15,
      child:  Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: DefaultPadding,
              right: DefaultPadding,
              bottom: 36 + DefaultPadding,
            ),
            height: size.height * 0.15 - 20,
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(36),
                bottomRight: Radius.circular(36),
              ),
            ),
            // child: Row(
            //   children: <Widget>[
            //     Text(
            //       'Hi!',
            //       style: Theme.of(context).textTheme.headline4.copyWith(
            //           color: Colors.white, fontWeight: FontWeight.bold),
            //     ),
            //     Image.asset("assets/images/logo_smile.png")
            //     // CachedNetworkImage(
            //     //   imageUrl: "https://images.unsplash.com/photo-1517423440428-a5a00ad493e8",
            //     //   placeholder: (context, url) => CircularProgressIndicator(),
            //     //   errorWidget: (context, url, error) => Icon(Icons.error),
            //     // ),
            //   ],
            // ),
          ),
          Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                  margin: EdgeInsets.symmetric(horizontal: DefaultPadding),
                  padding: EdgeInsets.symmetric(horizontal: DefaultPadding),
                  height: 47,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0,10),
                          blurRadius: 50,
                          color: primaryColor.withOpacity(0.23),
                        )
                      ]
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: "Search",
                              suffixIcon: Icon(Icons.search,color: primaryColor,),
                              hintStyle: TextStyle(
                                color: primaryColor.withOpacity(0.5),
                              ),
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none
                          ),

                        ),)
                    ],
                  )

              )
          )
        ],
      ),
    );
  }


}